#!/bin/bash
ACCOUNT_ID=344536799541
ES_DOMAIN_NAME="eksworkshop-logging"
kubectl delete -f ~/fluentbit.yaml 2>&1 | tee -a delete_opensearch_log.txt
aws opensearch delete-domain --domain-name $ES_DOMAIN_NAME 2>&1 | tee -a delete_opensearch_log.txt
eksctl delete iamserviceaccount --name fluent-bit --namespace logging --cluster mundose-eks-pin-cluster --wait 2>&1 | tee -a delete_opensearch_log.txt
aws iam delete-policy --policy-arn "arn:aws:iam::$ACCOUNT_ID:policy/fluent-bit-policy" 2>&1 | tee -a delete_opensearch_log.txt
kubectl delete namespace logging 2>&1 | tee -a delete_opensearch_log.txt