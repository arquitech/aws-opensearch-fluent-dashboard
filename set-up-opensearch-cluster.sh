#!/bin/bash
AWS_REGION='us-east-1'
echo "export AWS_REGION=$AWS_REGION" >> ~/.profile
export AWS_REGION=$AWS_REGION
ACCOUNT_ID=344536799541
echo "export ACCOUNT_ID=$ACCOUNT_ID" >> ~/.profile
export ACCOUNT_ID=$ACCOUNT_ID
ES_DOMAIN_NAME="eksworkshop-logging"
echo "export ES_DOMAIN_NAME=$ES_DOMAIN_NAME" >> ~/.profile
export ES_DOMAIN_NAME=$ES_DOMAIN_NAME
ES_VERSION="OpenSearch_1.0"
echo "export ES_VERSION=$ES_VERSION" >> ~/.profile
export ES_VERSION=$ES_VERSION
ES_DOMAIN_USER="eksworkshop"
echo "export ES_DOMAIN_USER=$ES_DOMAIN_USER" >> ~/.profile
export ES_DOMAIN_USER=$ES_DOMAIN_USER
ES_DOMAIN_PASSWORD="$(openssl rand -base64 12)_Ek1$"
echo "export ES_DOMAIN_PASSWORD=$ES_DOMAIN_PASSWORD" >> ~/.profile
export ES_DOMAIN_PASSWORD=$ES_DOMAIN_PASSWORD

# Create working directory
git clone https://bitbucket.org/arquitech/aws-opensearch-fluent-dashboard.git 2>&1 | tee -a set_up_opensearch_log.txt

#Enabling IAM roles for service accounts on your cluster
eksctl utils associate-iam-oidc-provider --cluster mundose-eks-pin-cluster --approve 2>&1 | tee -a set_up_opensearch_log.txt

#Creating an IAM role and policy for your service account
aws iam create-policy --policy-name fluent-bit-policy --policy-document file://~/aws-opensearch-fluent-dashboard/fluent-bit-policy.json  2>&1 | tee -a set_up_opensearch_log.txt

#Create an IAM role
kubectl create namespace logging  2>&1 | tee -a set_up_opensearch_log.txt
eksctl create iamserviceaccount --name fluent-bit --namespace logging --cluster mundose-eks-pin-cluster --attach-policy-arn "arn:aws:iam::$ACCOUNT_ID:policy/fluent-bit-policy" --approve --override-existing-serviceaccounts  2>&1 | tee -a set_up_opensearch_log.txt

#Make sure your service account with the ARN of the IAM role is annotated
kubectl -n logging describe sa fluent-bit 2>&1 | tee -a set_up_opensearch_log.txt

#PROVISION AN AMAZON OPENSEARCH CLUSTER
curl -Ss https://bitbucket.org/arquitech/aws-opensearch-fluent-dashboard/raw/ffea00c866fc3ddcc58ebef8f309a12d82254535/es_domain.json | envsubst > ~/es_domain.json
aws opensearch create-domain --cli-input-json  file://~/es_domain.json 2>&1 | tee -a set_up_opensearch_log.txt

BUILD_STATE="$(aws opensearch describe-domain --domain-name $ES_DOMAIN_NAME --query 'DomainStatus.Processing')"
while [ $BUILD_STATE == "true" ]
do
    sleep 10
    BUILD_STATE="$(aws opensearch describe-domain --domain-name $ES_DOMAIN_NAME --query 'DomainStatus.Processing')"
    echo "The Amazon OpenSearch cluster is NOT ready." 2>&1 | tee -a set_up_opensearch_log.txt
done

echo "The Amazon OpenSearch cluster is ready." 2>&1 | tee -a set_up_opensearch_log.txt
echo "export BUILD_STATE=$BUILD_STATE" >> ~/.profile

#Mapping Roles to Users
# We need to retrieve the Fluent Bit Role ARN
FLUENTBIT_ROLE="$(eksctl get iamserviceaccount --cluster mundose-eks-pin-cluster --namespace logging -o json | jq '.[].status.roleARN' -r)"
echo "export FLUENTBIT_ROLE=$FLUENTBIT_ROLE" >> ~/.profile
export FLUENTBIT_ROLE=$FLUENTBIT_ROLE

echo "Waiting for domain to be ready...."
sleep 120

# Get the Amazon OpenSearch Endpoint
ES_ENDPOINT="$(aws opensearch describe-domain --domain-name $ES_DOMAIN_NAME --output text --query "DomainStatus.Endpoint")"
echo "export ES_ENDPOINT=$ES_ENDPOINT" >> ~/.profile
export ES_ENDPOINT=$ES_ENDPOINT

# Update the Elasticsearch internal database
curl -sS -u "$ES_DOMAIN_USER:$ES_DOMAIN_PASSWORD" -X PATCH https://$ES_ENDPOINT/_opendistro/_security/api/rolesmapping/all_access?pretty -H 'Content-Type: application/json' -d'[{"op": "add", "path": "/backend_roles", "value": ["'$FLUENTBIT_ROLE'"]}]' 2>&1 | tee -a set_up_opensearch_log.txt

#DEPLOY FLUENT BIT
curl -Ss https://bitbucket.org/arquitech/aws-opensearch-fluent-dashboard/raw/49f55e0f5414dda3b56a8cbb9e051eda3db04a9d/fluentbit.yaml | envsubst > ~/fluentbit.yaml
kubectl apply -f ~/fluentbit.yaml 2>&1 | tee -a set_up_opensearch_log.txt
sleep 60
kubectl --namespace=logging get pods 2>&1 | tee -a set_up_opensearch_log.txt

#OPENSEARCH DASHBOARDS
echo "OpenSearch Dashboards URL: https://$ES_ENDPOINT/_dashboards/ OpenSearch Dashboards user: $ES_DOMAIN_USER OpenSearch Dashboards password: $ES_DOMAIN_PASSWORD" 2>&1 | tee -a set_up_opensearch_log.txt